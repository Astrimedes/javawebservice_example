package turano.example;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {

    /**
     * Request a random list of person info
     * @param args use 'debug' to deserialize and write to console, or use an absolute path file directory to write a persons.json file, or no args will write file to working dir
     * @throws IOException request problem
     */
    public static void main(String[] args) throws IOException {

        final String url = "https://randomuser.me/api/?results=30&nat=US";

        System.out.println("Requesting content from " +  url);
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(url);
        CloseableHttpResponse response = httpclient.execute(httpGet);

        try {
            // check status of response
            StatusLine status = response.getStatusLine();
            System.out.println("Response from " + url + ": " + status);

            // abort if not successful
            if (status.getStatusCode() != 200) {
                System.out.println("Response not successful. Exiting.");
                return;
            }

            // read content
            HttpEntity entity = response.getEntity();
            String contentString = EntityUtils.toString(entity);

            // if debug argument, deserialize and write info to console
            if (args.length > 0 && args[0].equals("debug")) {
                // (used generated classes for deserialization from http://www.jsonschema2pojo.org/ results)
                ObjectMapper objectMapper = new ObjectMapper();
                Results results = objectMapper.readValue(contentString, Results.class);

                // print name & email of each person
                for(Result r : results.getResults()) {
                    System.out.println(r.getName().getFirst() + " " + r.getName().getLast() + ": " + r.getEmail());
                }

                return;
            }

            // if no parameters, write to working dir, otherwise write to absolute path
            String filePath = "";
            if (args.length > 0 && args[0].length() > 3) {
                filePath = args[0];
            }
            boolean success = writeToFile(filePath, contentString);
            if (!success) {
                System.out.println("Could not save file!");
            }

            // ensure it's fully consumed, per Apache guide example
            EntityUtils.consume(entity);
        } finally {
            // guarantee close
            response.close();
        }
    }

    /**
     * Write to the file system
     * @param fullpath path to write the file to. use "" for current working dir
     * @param contents json file content
     * @return
     */
    private  static boolean writeToFile(String fullpath, String contents) {
        File file;
        // use current relative path if null
        if (fullpath.equals("")) {
            fullpath = Paths.get("").toAbsolutePath().toString();
        }

        file = new File(fullpath, "persons.json");

        boolean wrote = false;
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(file.getAbsoluteFile()));
            bw.write(contents);
            bw.close();
            wrote = true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (wrote) {
            System.out.println("Saved content as " + file.getAbsoluteFile());
        }

        return wrote;
    }


}
